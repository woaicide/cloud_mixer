#include "composition.h"
#include "render/layer.h"
#include "render/d3d11.h"

Composition::Composition(const std::shared_ptr<IDevice> & device, int width, int height)
  : m_width(width)
  , m_height(height)
  , m_device(device)
{
  m_device->CreateSwapBuffer(m_width, m_height);
}

Composition::~Composition()
{

}

int Composition::Width() const
{
  return m_width;
}

int Composition::Height() const
{
  return m_height;
}

void Composition::Tick(uint64_t t)
{
  MSG msg = {};
  if (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE))
  {
    if (!TranslateAccelerator(msg.hwnd, NULL, &msg))
    {
      TranslateMessage(&msg);
      DispatchMessage(&msg);
    }
  }

  decltype(m_layers) layers;
  {
    std::lock_guard<std::mutex> guard(m_lock);
    layers.assign(m_layers.begin(), m_layers.end());
  }

  for (const auto & layer : layers)
  {
    layer->Tick(t);
  }

  m_device->PreSwapBuffer();
  Render(m_device);
  m_device->SwapBuffer();
}

void Composition::Render(const std::shared_ptr<IDevice> & device)
{
  decltype(m_layers) layers;
  {
    std::lock_guard<std::mutex> guard(m_lock);
    layers.assign(m_layers.begin(), m_layers.end());
  }

  for (auto const& layer : layers)
  {
    layer->Render(device);
  }
}

void Composition::AddLayer(const std::shared_ptr<Layer> & layer)
{
  if (layer)
  {
    std::lock_guard<std::mutex> guard(m_lock);

    m_layers.push_back(layer);
    layer->Attach(shared_from_this());
  }
}

bool Composition::RemoveLayer(const std::shared_ptr<Layer> & layer)
{
  uint32_t match = 0;
  if (layer)
  {
    std::lock_guard<std::mutex> guard(m_lock);
    for (auto i = m_layers.begin(); i != m_layers.end(); )
    {
      if ((*i).get() == layer.get())
      {
        i = m_layers.erase(i);
        ++match;
      }
      else
      {
        i++;
      }
    }
  }
  return (match > 0);
}

void Composition::Resize(int width, int height)
{
  m_width = width;
  m_height = height;
}

std::shared_ptr<IDevice> Composition::GetDevice()
{
  return m_device;
}

std::shared_ptr<Composition> CreateComposition(int width, int height)
{
  auto device = CreateDevice();
  if (!device)
    return nullptr;
  return std::make_shared<Composition>(device, width, height);
}
