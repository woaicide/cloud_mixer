#ifndef PLATFORM_H_
#define PLATFORM_H_

#include <stdint.h>

#if defined(OS_WIN) || defined(_WIN32)
#include <windows.h>
#include <objbase.h>
#include <Shellapi.h>
#include <Shlwapi.h>
#include <ShlObj.h>
#include <assert.h>
#define ModuleHandle HINSTANCE
#endif

#ifdef OS_LINUX
#define ModuleHandle void*
#endif

#endif  // PLATFORM_H_
