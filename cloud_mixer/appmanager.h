#ifndef APPMANAGER_H
#define APPMANAGER_H
#include "platform.h"
#include <uv.h>
#include "composition.h"
#include "base/time/time.h"

#define NO_ERROR 0
class AppManager
{
public:
  AppManager();
  ~AppManager();
  static AppManager& Instance();
  static void IdleLoopCb(uv_idle_t* handle);

  int Init();
  void RunLoop();
  void Close();
  void IdelLoop(uv_idle_t* handle);

private:
  int DoInitComposition();
  void DoRenderLoop();

private:
  uv_loop_t m_loop;
  uv_idle_t m_idler;
  bool m_exitLoop = false;
  int m_loopMillSec = 10;
  base::TimeTicks m_lastTick;

  std::shared_ptr<Composition> m_composition;
};
#endif //APPMANAGER_H