#include "cef_moudle.h"
#include <include/cef_version.h>
#include <sstream>
#include <functional>

std::shared_ptr<CefModule> CefModule::g_instance;
CefModule::CefModule(ModuleHandle mod)
  : m_module(mod)
{
  m_ready = false;
}

CefModule::~CefModule()
{

}

void CefModule::Startup(ModuleHandle mod)
{
  g_instance = std::make_shared<CefModule>(mod);
  g_instance->m_thread = std::make_shared<std::thread>(
    std::bind(&CefModule::MessageLoop, g_instance.get()));

  { 
    std::unique_lock<std::mutex> lock(g_instance->m_lock);
    std::weak_ptr<CefModule> weak_self(g_instance);
    g_instance->m_signal.wait(lock, [weak_self]() {
      auto const mod = weak_self.lock();
      if (mod) {
        return mod->m_ready.load();
      }
      return true;
    });
  }

  LOG(INFO) << "cef module is ready\n";
}

void CefModule::Shutdown()
{
  if (g_instance)
  {
    if (g_instance->m_thread)
    {
      CefRefPtr<CefTask> task(new QuitTask());
      CefPostTask(TID_UI, task.get());
      g_instance->m_thread->join();
      g_instance->m_thread.reset();
    }
    g_instance.reset();
  }
}

void CefModule::MessageLoop()
{
  LOG(INFO) << "cef initializing ...";

  CefSettings settings;
  settings.no_sandbox = true;
  settings.multi_threaded_message_loop = false;
  settings.windowless_rendering_enabled = true;

  CefRefPtr<CefAppWrapper> app(new CefAppWrapper());

  CefMainArgs main_args(m_module);
  CefInitialize(main_args, settings, app, nullptr);

  LOG(INFO) << "cef is initialized.";

  // signal cef is initialized and ready
  m_ready = true;
  m_signal.notify_one();

  CefRunMessageLoop();

  LOG(INFO) << "cef shutting down ...";

  CefShutdown();
  LOG(INFO) << "cef is shutdown";
}

int CefInitialize(ModuleHandle instance)
{
  CefEnableHighDPISupport();

  {
    CefRefPtr<CefAppWrapper> app(new CefAppWrapper());
    CefMainArgs main_args(instance);
    int exit_code = CefExecuteProcess(main_args, app, nullptr);
    if (exit_code >= 0)
      return exit_code;
  }

  CefModule::Startup(instance);
  return -1;
}

void CefUninitialize()
{
  CefModule::Shutdown();
}

std::string CefVersion()
{
  std::ostringstream ver;
  ver << "CEF: " <<
    CEF_VERSION << " (Chromium: "
    << CHROME_VERSION_MAJOR << "."
    << CHROME_VERSION_MINOR << "."
    << CHROME_VERSION_BUILD << "."
    << CHROME_VERSION_PATCH << ")";
  return ver.str();
}

CefAppWrapper::CefAppWrapper()
{

}

CefRefPtr<CefBrowserProcessHandler> CefAppWrapper::GetBrowserProcessHandler()
{
  return this;
}

CefRefPtr<CefRenderProcessHandler> CefAppWrapper::GetRenderProcessHandler()
{
  return m_renderProcessHandler;
}

void CefAppWrapper::OnBeforeCommandLineProcessing(CefString const& process_type, CefRefPtr<CefCommandLine> command_line)
{
  command_line->AppendSwitch("disable-gpu-shader-disk-cache");
  //command_line->AppendSwitch("disable-accelerated-video-decode");
  //command_line->AppendSwitch("disable-gpu-vsync");

  command_line->AppendSwitchWithValue("use-angle", "d3d11");
  command_line->AppendSwitchWithValue("autoplay-policy", "no-user-gesture-required");

  //command_line->AppendSwitch("single-process");
  //command_line->AppendSwitch("show-fps-counter");
  command_line->AppendSwitchWithValue("ppapi-flash-version", "32.0.0.207");
  command_line->AppendSwitchWithValue("ppapi-flash-path", "PepperFlash\\win32\\pepflashplayer32_32_0_0_207.dll");
}
