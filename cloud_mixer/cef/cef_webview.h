#ifndef CEFWEBVIEW_H
#define CEFWEBVIEW_H

#include <mutex>
#include "base/time/time.h"
#include <include/cef_browser.h>
#include <include/cef_client.h>
#include "platform.h"
#include "render/framebuffer.h"
#include "render/render.h"

class WebView : public CefClient, public CefRenderHandler, public CefLifeSpanHandler, public CefLoadHandler
{
public:
  WebView(const std::string & url, const std::shared_ptr<IDevice> & device, int width, int height
    , bool useSharedTextures, bool sendBeginFrame);

  ~WebView();

  bool UseSharedTextures() const;
  void Close();
  std::shared_ptr<ITexture2D> GetTexture(const std::shared_ptr<IDevice> & device);
  void RequestFrame();
  void Resize(int width, int height);

  //CefClient impl
  CefRefPtr<CefRenderHandler> GetRenderHandler() override;
  CefRefPtr<CefLifeSpanHandler> GetLifeSpanHandler() override;
  CefRefPtr<CefLoadHandler> GetLoadHandler() override;

  //CefRenderHandler impl
  void OnPaint(CefRefPtr<CefBrowser> browser, PaintElementType type,const RectList& dirtyRects, const void* buffer, int width, int height) override;
  void OnAcceleratedPaint(CefRefPtr<CefBrowser> browser, PaintElementType type, const RectList& dirtyRects, void* share_handle) override;
  void GetViewRect(CefRefPtr<CefBrowser> /*browser*/, CefRect& rect) override;

  //CefLifeSpanHandler impl
  bool OnBeforePopup(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, const CefString& target_url, const CefString& target_frame_name,
    WindowOpenDisposition target_disposition, bool user_gesture, const CefPopupFeatures& popupFeatures,  CefWindowInfo& windowInfo,
    CefRefPtr<CefClient>& client, CefBrowserSettings& settings, bool* no_javascript_access) override;
  void OnBeforeClose(CefRefPtr<CefBrowser> browser) override;
  void OnAfterCreated(CefRefPtr<CefBrowser> browser) override;

  //CefLoadHandler impl
  void OnLoadEnd(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, int httpStatusCode) override;
  void OnLoadError(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, ErrorCode errorCode, const CefString& errorText, const CefString& failedUrl) override;

private:
  IMPLEMENT_REFCOUNTING(WebView);
  CefRefPtr<CefBrowser> SafeBrowser();

  std::string m_url;
  int m_width;
  int m_height;
  std::shared_ptr<FrameBuffer> m_viewFrameBuffer;
  std::mutex m_lock;
  CefRefPtr<CefBrowser> m_browser;
  bool m_useSharedTextures;
  bool m_sendBeginFrame;
  uint64_t m_frameCount = 0;
  base::TimeTicks m_lastTime;
};

#endif //CEFWEBVIEW_H