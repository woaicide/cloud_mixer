#ifndef CEFMOUDLE_H
#define CEFMOUDLE_H

#include <mutex>
#include <condition_variable>
#include <atomic>
#include <thread>
#include "include/cef_task.h"
#include <include/cef_app.h>
#include "platform.h"

int CefInitialize(ModuleHandle instance);
void CefUninitialize();
std::string CefVersion();

class CefModule
{
public:
  CefModule(ModuleHandle mod);
  ~CefModule();

  static void Startup(ModuleHandle);
  static void Shutdown();

private:
  class QuitTask : public CefTask
  {
  public:
    QuitTask() { }
    void Execute() override
    {
      CefQuitMessageLoop();
    }
    IMPLEMENT_REFCOUNTING(QuitTask);
  };

  void MessageLoop();

  std::condition_variable m_signal;
  std::atomic_bool m_ready;
  std::mutex m_lock;
  const ModuleHandle m_module;
  std::shared_ptr<std::thread> m_thread;
  static std::shared_ptr<CefModule> g_instance;
};

class CefAppWrapper : public CefApp, public CefBrowserProcessHandler
{
public:
  CefAppWrapper();

  CefRefPtr<CefBrowserProcessHandler> GetBrowserProcessHandler() override;
  CefRefPtr<CefRenderProcessHandler> GetRenderProcessHandler() override;
  void OnBeforeCommandLineProcessing(CefString const& process_type, CefRefPtr<CefCommandLine> command_line) override;

private:
  IMPLEMENT_REFCOUNTING(CefAppWrapper);
  CefRefPtr<CefRenderProcessHandler> m_renderProcessHandler;
};
#endif //CEFMOUDLE_H