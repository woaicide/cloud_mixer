#include "cef_webview.h"
#include "include/cef_task.h"

WebView::WebView(const std::string & url, const std::shared_ptr<IDevice> & device, int width, int height, bool useSharedTextures, bool sendBeginFrame)
  : m_url(url)
  , m_width(width)
  , m_height(height)
  , m_viewFrameBuffer(std::make_shared<FrameBuffer>(device))
  , m_useSharedTextures(useSharedTextures)
  , m_sendBeginFrame(sendBeginFrame)
{

}

WebView::~WebView()
{
  Close();
}

bool WebView::UseSharedTextures() const
{
  return m_useSharedTextures;
}

void WebView::Close()
{
  decltype(m_browser) browser;
  {
    std::lock_guard<std::mutex> guard(m_lock);
    browser = m_browser;
    m_browser = nullptr;
  }

  if (browser.get()) 
  {
    browser->GetHost()->CloseBrowser(true);
  }
}

std::shared_ptr<ITexture2D> WebView::GetTexture(const std::shared_ptr<IDevice> & device)
{
  if (m_viewFrameBuffer) 
  {
    return m_viewFrameBuffer->GetTexture(device);
  }
  return std::shared_ptr<ITexture2D>();
}

void WebView::RequestFrame()
{
  auto const browser = SafeBrowser();
  if (m_sendBeginFrame && browser)
  {
    browser->GetHost()->SendExternalBeginFrame();
  }
}

void WebView::Resize(int width, int height)
{
  if (width != m_width || height != m_height)
  {
    m_width = width;
    m_height = height;

    auto const browser = SafeBrowser();
    if (browser)
    {
      browser->GetHost()->WasResized();
      LOG(INFO) << "browser resize width:" << width << " height:" << height;
    }
  }
}

CefRefPtr<CefRenderHandler> WebView::GetRenderHandler()
{
  return this;
}

CefRefPtr<CefLifeSpanHandler> WebView::GetLifeSpanHandler()
{
  return this;
}

CefRefPtr<CefLoadHandler> WebView::GetLoadHandler()
{
  return this;
}

void WebView::OnPaint(CefRefPtr<CefBrowser> browser, PaintElementType type, const RectList& dirtyRects, const void* buffer, int width, int height)
{
  if (type == PET_VIEW)
  {
    if (m_viewFrameBuffer)
    {
      m_viewFrameBuffer->OnPaint(buffer, width, height);
    }
    ++m_frameCount;
    base::TimeTicks current = base::TimeTicks::Now();
    base::TimeDelta delta = current - m_lastTime;
    if (delta.InMilliseconds() > 3000)
    {
      LOG(INFO) << "OnPaint url: " << m_url << " FPS: " << m_frameCount * 1000 / delta.InMilliseconds();
      m_frameCount = 0;
      m_lastTime = current;
    }
  }
  else
  {
    LOG(INFO) << "PET_POPUP log";
  }
}

void WebView::OnAcceleratedPaint(CefRefPtr<CefBrowser> browser, PaintElementType type, const RectList& dirtyRects, void* share_handle)
{
  if (type == PET_VIEW)
  {
    if (m_viewFrameBuffer) 
    {
      m_viewFrameBuffer->OnGpuPaint((void*)share_handle);
    }
    ++m_frameCount;
    base::TimeTicks current = base::TimeTicks::Now();
    base::TimeDelta delta = current - m_lastTime;
    if (delta.InMilliseconds() > 3000)
    {
      LOG(INFO) << "OnAcceleratedPaint url: " << m_url << " FPS: " << m_frameCount * 1000 / delta.InMilliseconds();
      m_frameCount = 0;
      m_lastTime = current;
    }
  }
  else
  {
    LOG(INFO) << "PET_POPUP log";
  }
}

void WebView::GetViewRect(CefRefPtr<CefBrowser> /*browser*/, CefRect& rect)
{
  rect.Set(0, 0, m_width, m_height);
}

void WebView::OnAfterCreated(CefRefPtr<CefBrowser> browser)
{
  if (!CefCurrentlyOn(TID_UI))
  {
    assert(0);
    return;
  }

  {
    std::lock_guard<std::mutex> guard(m_lock);
    if (!m_browser.get())
    {
      m_browser = browser;
    }
  }
}

bool WebView::OnBeforePopup(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, const CefString& target_url, const CefString& target_frame_name,
  WindowOpenDisposition target_disposition, bool user_gesture, const CefPopupFeatures& popupFeatures, CefWindowInfo& windowInfo,
  CefRefPtr<CefClient>& client, CefBrowserSettings& settings, bool* no_javascript_access)
{
  return true;
}

void WebView::OnBeforeClose(CefRefPtr<CefBrowser> browser)
{
  LOG(INFO) << "Browser is closed, url: " << browser->GetMainFrame()->GetURL().ToString();
}

void WebView::OnLoadEnd(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, int httpStatusCode)
{

}

void WebView::OnLoadError(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, ErrorCode errorCode, const CefString& errorText, const CefString& failedUrl)
{

}

CefRefPtr<CefBrowser> WebView::SafeBrowser()
{
  std::lock_guard<std::mutex> guard(m_lock);
  return m_browser;
}
