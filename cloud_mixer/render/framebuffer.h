#ifndef FRAMEBUFFER_H
#define FRAMEBUFFER_H

#include <memory>
#include <string>
#include <mutex>
#include "../platform.h"
#include "render/render.h"

class FrameBuffer
{
public:
  FrameBuffer(const std::shared_ptr<IDevice> & device);

  int32_t Width();
  int32_t Height();

  void OnPaint(const void* buffer, uint32_t width, uint32_t height);
  void OnGpuPaint(void* shared_handle);
  std::shared_ptr<ITexture2D> GetTexture(const std::shared_ptr<IDevice>& device);

private:
  std::mutex m_lock;
  std::shared_ptr<ITexture2D> m_sharedTexture;
  const std::shared_ptr<IDevice> m_device;
  std::shared_ptr<uint8_t> m_buffer;
  bool m_isDirty;
};
#endif //FRAMEBUFFER_H