#include "framebuffer.h"
#include "d3d11.h"
#include "base/logging.h"

FrameBuffer::FrameBuffer(const std::shared_ptr<IDevice> & device)
  : m_device(device)
  , m_isDirty(false)
{

}

int32_t FrameBuffer::Width()
{
  if (m_sharedTexture)
  {
    return m_sharedTexture->Width();
  }
  return 0;
}

int32_t FrameBuffer::Height()
{
  if (m_sharedTexture)
  {
    return m_sharedTexture->Height();
  }
  return 0;
}

void FrameBuffer::OnPaint(const void* buffer, uint32_t width, uint32_t height)
{
  uint32_t cb = width * 4 * height;

  std::lock_guard<std::mutex> guard(m_lock);
  if (!m_sharedTexture || (m_sharedTexture->Width() != width) || (m_sharedTexture->Height() != height))
  {
    m_sharedTexture = m_device->CreateTexture(width, height, DXGI_FORMAT_B8G8R8A8_UNORM, nullptr, 0);

    m_buffer = std::shared_ptr<uint8_t>((uint8_t*)malloc(cb), free);
  }

  if (m_buffer && buffer)
  {
    memcpy(m_buffer.get(), buffer, cb);
  }

  m_isDirty = true;
}

void FrameBuffer::OnGpuPaint(void* shared_handle)
{
  std::lock_guard<std::mutex> guard(m_lock);

  if (m_sharedTexture)
  {
    if (shared_handle != m_sharedTexture->ShareHandle()) 
    {
      m_sharedTexture.reset();
    }
  }

  if (!m_sharedTexture)
  {
    m_sharedTexture = m_device->OpenSharedTexture((void*)shared_handle);
    if (!m_sharedTexture) 
    {
      LOG(INFO) << "could not open shared texture!";
    }
  }

  m_isDirty = true;
}

std::shared_ptr<ITexture2D> FrameBuffer::GetTexture(const std::shared_ptr<IDevice>& device)
{
  std::lock_guard<std::mutex> guard(m_lock);
  if (m_buffer && m_sharedTexture && m_isDirty)
  {
    ScopedBinder<ITexture2D> binder(device, m_sharedTexture);
    m_sharedTexture->CopyFrom(m_buffer.get(), m_sharedTexture->Width() * 4, m_sharedTexture->Height());
  }

  m_isDirty = false;
  return m_sharedTexture;
}