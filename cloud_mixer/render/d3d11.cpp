#include "d3d11.h"

#include <d3dcompiler.h>
#include <directxmath.h>
#include "base/logging.h"

using namespace std;

struct SimpleVertex
{
  DirectX::XMFLOAT3 pos;
  DirectX::XMFLOAT2 tex;
};

template<class T>
std::shared_ptr<T> to_com_ptr(T* obj)
{
  return std::shared_ptr<T>(obj, [](T* p) { if (p) p->Release(); });
}

SwapChain::SwapChain(IDXGISwapChain* swapchain, ID3D11RenderTargetView* rtv, ID3D11SamplerState* sampler, ID3D11BlendState* blender)
  : m_sampler(to_com_ptr(sampler))
  , m_blender(to_com_ptr(blender))
  , m_swapchain(to_com_ptr(swapchain))
  , m_rtv(to_com_ptr(rtv))
{
}

SwapChain::~SwapChain()
{

}

void SwapChain::Bind(const shared_ptr<IDevice>& device)
{
  m_device = device;
  ID3D11DeviceContext* d3d11_ctx = (ID3D11DeviceContext*)(m_device.lock()->GetContext());

  ID3D11RenderTargetView* rtv[1] = { m_rtv.get() };
  d3d11_ctx->OMSetRenderTargets(1, rtv, nullptr);

  // set default blending state
  if (m_blender)
  {
    float factor[4] = { 0.0f, 0.0f, 0.0f, 0.0f };
    d3d11_ctx->OMSetBlendState(m_blender.get(), factor, 0xffffffff);
  }

  // set default sampler state
  if (m_sampler)
  {
    ID3D11SamplerState* samplers[1] = { m_sampler.get() };
    d3d11_ctx->PSSetSamplers(0, 1, samplers);
  }
}

void SwapChain::UnBind()
{

}

void SwapChain::SwapBuffer()
{
  Present(0);
}

void SwapChain::Clear(float red, float green, float blue, float alpha)
{
  ID3D11DeviceContext* d3d11_ctx = (ID3D11DeviceContext*)(m_device.lock()->GetContext());
  assert(d3d11_ctx);

  FLOAT color[4] = { red, green, blue, alpha };
  d3d11_ctx->ClearRenderTargetView(m_rtv.get(), color);
}

void SwapChain::Present(int sync_interval)
{
  m_swapchain->Present(sync_interval, 0);
}

void SwapChain::Resize(int width, int height)
{
  if (width <= 0 || height <= 0)
    return;

  ID3D11DeviceContext* d3d11_ctx = (ID3D11DeviceContext*)(m_device.lock()->GetContext());
  assert(d3d11_ctx);

  d3d11_ctx->OMSetRenderTargets(0, 0, 0);
  m_rtv.reset();

  DXGI_SWAP_CHAIN_DESC desc;
  m_swapchain->GetDesc(&desc);
  auto hr = m_swapchain->ResizeBuffers(0, width, height, desc.BufferDesc.Format, desc.Flags);
  if (FAILED(hr))
  {
    LOG(ERROR) << "failed to resize swapchain width:" << width << " height:" << height;
    return;
  }

  ID3D11Texture2D* buffer = nullptr;
  hr = m_swapchain->GetBuffer(0, __uuidof(ID3D11Texture2D), (void**)&buffer);
  if (FAILED(hr))
  {
    LOG(ERROR) << "failed to resize swapchain width:" << width << " height:" << height;
    return;
  }

  ID3D11Device* dev = nullptr;
  d3d11_ctx->GetDevice(&dev);
  if (dev)
  {
    D3D11_RENDER_TARGET_VIEW_DESC vdesc = {};
    vdesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
    vdesc.Texture2D.MipSlice = 0;
    vdesc.Format = desc.BufferDesc.Format;

    ID3D11RenderTargetView* view = nullptr;
    hr = dev->CreateRenderTargetView(buffer, &vdesc, &view);
    if (SUCCEEDED(hr))
    {
      m_rtv = to_com_ptr(view);
      d3d11_ctx->OMSetRenderTargets(1, &view, nullptr);
    }
    dev->Release();
  }
  buffer->Release();

  D3D11_VIEWPORT vp;
  vp.Width = static_cast<float>(width);
  vp.Height = static_cast<float>(height);
  vp.MinDepth = D3D11_MIN_DEPTH;
  vp.MaxDepth = D3D11_MAX_DEPTH;
  vp.TopLeftX = 0;
  vp.TopLeftY = 0;
  d3d11_ctx->RSSetViewports(1, &vp);
}


Effect::Effect(
  ID3D11VertexShader* vsh,
  ID3D11PixelShader* psh,
  ID3D11InputLayout* layout)
  : m_vertexShader(to_com_ptr(vsh))
  , m_pixelShader(to_com_ptr(psh))
  , m_layout(to_com_ptr(layout))
{
}

void Effect::Bind(const shared_ptr<IDevice>& ctx)
{
  m_device = ctx;
  ID3D11DeviceContext* d3d11_ctx = (ID3D11DeviceContext*)(m_device.lock()->GetContext());

  d3d11_ctx->IASetInputLayout(m_layout.get());
  d3d11_ctx->VSSetShader(m_vertexShader.get(), nullptr, 0);
  d3d11_ctx->PSSetShader(m_pixelShader.get(), nullptr, 0);
}

void Effect::UnBind()
{
}

Geometry::Geometry(D3D_PRIMITIVE_TOPOLOGY primitive, uint32_t vertices, uint32_t stride, ID3D11Buffer* buffer)
  : m_primitive(primitive)
  , m_vertices(vertices)
  , m_stride(stride)
  , m_buffer(to_com_ptr(buffer))
{
}

Geometry::~Geometry()
{

}

void Geometry::Bind(const shared_ptr<IDevice>& device)
{
  m_device = device;
  ID3D11DeviceContext* d3d11_ctx = (ID3D11DeviceContext*)(device->GetContext());

  // todo: handle offset
  uint32_t offset = 0;

  ID3D11Buffer* buffers[1] = { m_buffer.get() };
  d3d11_ctx->IASetVertexBuffers(0, 1, buffers, &m_stride, &offset);
  d3d11_ctx->IASetPrimitiveTopology(m_primitive);
}

void Geometry::UnBind()
{
}

void Geometry::Draw()
{
  ID3D11DeviceContext* d3d11_ctx = (ID3D11DeviceContext*)(m_device.lock()->GetContext());
  assert(d3d11_ctx);

  // todo: handle offset
  d3d11_ctx->Draw(m_vertices, 0);
}

Texture2D::Texture2D(ID3D11Texture2D* tex, ID3D11ShaderResourceView* srv)
  : m_texture(to_com_ptr(tex))
  , m_shaderResourceView(to_com_ptr(srv))
{
  m_shareHandle = nullptr;

  IDXGIResource* res = nullptr;
  if (SUCCEEDED(m_texture->QueryInterface(
    __uuidof(IDXGIResource), reinterpret_cast<void**>(&res))))
  {
    res->GetSharedHandle(&m_shareHandle);
    res->Release();
  }
}

Texture2D::~Texture2D()
{

}

uint32_t Texture2D::Width() const
{
  D3D11_TEXTURE2D_DESC desc;
  m_texture->GetDesc(&desc);
  return desc.Width;
}

uint32_t Texture2D::Height() const
{
  D3D11_TEXTURE2D_DESC desc;
  m_texture->GetDesc(&desc);
  return desc.Height;
}

int Texture2D::Format() const
{
  D3D11_TEXTURE2D_DESC desc;
  m_texture->GetDesc(&desc);
  return desc.Format;
}

void Texture2D::Bind(const shared_ptr<IDevice> & device)
{
  m_device = device;
  ID3D11DeviceContext* d3d11_ctx = (ID3D11DeviceContext*)(m_device.lock()->GetContext());
  if (m_shaderResourceView)
  {
    ID3D11ShaderResourceView* views[1] = { m_shaderResourceView.get() };
    d3d11_ctx->PSSetShaderResources(0, 1, views);
  }
}

void Texture2D::UnBind()
{
}

void* Texture2D::ShareHandle() const
{
  return m_shareHandle;
}

void Texture2D::CopyFrom(shared_ptr<Texture2D> const& other)
{
  ID3D11DeviceContext* d3d11_ctx = (ID3D11DeviceContext*)(m_device.lock()->GetContext());
  assert(d3d11_ctx);
  if (other)
  {
    d3d11_ctx->CopyResource(m_texture.get(), other->m_texture.get());
  }
}

void Texture2D::CopyFrom(const void* buffer, uint32_t stride, uint32_t rows)
{
  if (!buffer) {
    return;
  }

  ID3D11DeviceContext* d3d11_ctx = (ID3D11DeviceContext*)(m_device.lock()->GetContext());
  assert(d3d11_ctx);

  D3D11_MAPPED_SUBRESOURCE res;
  auto const hr = d3d11_ctx->Map(m_texture.get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &res);
  if (SUCCEEDED(hr))
  {
    if (rows == Height())
    {
      if (res.RowPitch == stride)
      {
        memcpy(res.pData, buffer, stride * rows);
      }
      else
      {
        const uint8_t* src = (const uint8_t*)buffer;
        uint8_t* dst = (uint8_t*)res.pData;
        uint32_t cb = res.RowPitch < stride ? res.RowPitch : stride;
        for (uint32_t y = 0; y < rows; ++y)
        {
          memcpy(dst, src, cb);
          src += stride;
          dst += res.RowPitch;
        }
      }
    }

    d3d11_ctx->Unmap(m_texture.get(), 0);
  }
}

Device::Device(ID3D11Device* pdev, ID3D11DeviceContext* pctx)
  : m_device(to_com_ptr(pdev))
  , m_context(to_com_ptr(pctx))
{
  m_libCompiler = LoadLibrary(L"d3dcompiler_47.dll");
}

Device::~Device()
{

}

wstring Device::GetAdaptername()
{
  IDXGIDevice* dxgi_dev = nullptr;
  auto hr = m_device->QueryInterface(__uuidof(dxgi_dev), (void**)&dxgi_dev);
  if (SUCCEEDED(hr))
  {
    IDXGIAdapter* dxgi_adapt = nullptr;
    hr = dxgi_dev->GetAdapter(&dxgi_adapt);
    dxgi_dev->Release();
    if (SUCCEEDED(hr))
    {
      DXGI_ADAPTER_DESC desc;
      hr = dxgi_adapt->GetDesc(&desc);
      dxgi_adapt->Release();
      if (SUCCEEDED(hr))
      {
        return desc.Description;
      }
    }
  }

  return L"n/a";
}

void* Device::GetContext()
{
  return m_context.get();
}

static const wchar_t * WindowClass()
{
  static const wchar_t * ret = []()
  {
    static const wchar_t ret[] = L"BumblingWindow";

    WNDCLASSEX wcx;

    wcx.cbSize = sizeof(wcx);
    wcx.style = CS_HREDRAW | CS_VREDRAW;
    wcx.lpfnWndProc = DefWindowProc;
    wcx.cbClsExtra = 0;
    wcx.cbWndExtra = 0;
    wcx.hInstance = GetModuleHandle(NULL);
    wcx.hIcon = LoadIcon(nullptr, IDI_APPLICATION);
    wcx.hCursor = LoadCursor(NULL, IDC_ARROW);
    wcx.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
    wcx.lpszMenuName = nullptr;
    wcx.lpszClassName = ret;
    wcx.hIconSm = nullptr;

    // Register the window class. 
    return RegisterClassEx(&wcx) != FALSE ? ret : nullptr;
  }();

  return ret;
}

LRESULT CALLBACK WindowProc(HWND hwnd, UINT message, WPARAM wp, LPARAM lp)
{
  return DefWindowProc(hwnd, message, wp, lp);
}

void Device::CreateSwapBuffer(int width, int height)
{
  HRESULT hr;
  IDXGIFactory1* dxgi_factory = nullptr;

  std::wstring title(L"Mixer [gpu: ");
  title.append(GetAdaptername());
  title.append(L"]");
  const auto hwnd = CreateWindow(WindowClass(), title.c_str(), WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, 0, CW_USEDEFAULT, 0,
    nullptr, nullptr, GetModuleHandle(NULL), (LPVOID)this);
  if (!IsWindow(hwnd))
  {
    return;
  }

  RECT rc_outer, rc_inner;
  GetWindowRect(hwnd, &rc_outer);
  GetClientRect(hwnd, &rc_inner);
  SetWindowPos(hwnd, nullptr, 0, 0,
    width + ((rc_outer.right - rc_outer.left) - (rc_inner.right - rc_inner.left)),
    height + ((rc_outer.bottom - rc_outer.top) - (rc_inner.bottom - rc_inner.top)),
    SWP_NOMOVE | SWP_NOZORDER);
  ShowWindow(hwnd, SW_SHOWNORMAL);

  RECT rc_bounds;
  GetClientRect(hwnd, &rc_bounds);
  width = rc_bounds.right - rc_bounds.left;
  height = rc_bounds.bottom - rc_bounds.top;

  {
    IDXGIDevice* dxgi_dev = nullptr;
    hr = m_device->QueryInterface(__uuidof(IDXGIDevice), reinterpret_cast<void**>(&dxgi_dev));
    if (FAILED(hr))
    {
      return;
    }

    IDXGIAdapter* adapter = nullptr;
    hr = dxgi_dev->GetAdapter(&adapter);
    dxgi_dev->Release();
    if (FAILED(hr))
    {
      return;
    }

    hr = adapter->GetParent(__uuidof(IDXGIFactory1), reinterpret_cast<void**>(&dxgi_factory));
    adapter->Release();
  }

  if (!dxgi_factory)
  {
    return;
  }

  IDXGISwapChain* swapchain = nullptr;

  // Create swap chain
  IDXGIFactory2* dxgi_factory2 = nullptr;
  hr = dxgi_factory->QueryInterface(__uuidof(IDXGIFactory2), reinterpret_cast<void**>(&dxgi_factory2));
  if (dxgi_factory2)
  {
    DXGI_SWAP_CHAIN_DESC1 sd;
    ZeroMemory(&sd, sizeof(sd));
    sd.Width = width;
    sd.Height = height;
    sd.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
    sd.SampleDesc.Count = 1;
    sd.SampleDesc.Quality = 0;
    sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
    sd.BufferCount = 1;

    IDXGISwapChain1* swapchain1 = nullptr;
    hr = dxgi_factory2->CreateSwapChainForHwnd(
      m_device.get(), hwnd, &sd, nullptr, nullptr, &swapchain1);
    if (SUCCEEDED(hr))
    {
      hr = swapchain1->QueryInterface(__uuidof(IDXGISwapChain), reinterpret_cast<void**>(&swapchain));
      swapchain1->Release();
    }

    dxgi_factory2->Release();
  }
  else
  {
    // DirectX 11.0 systems
    DXGI_SWAP_CHAIN_DESC sd;
    ZeroMemory(&sd, sizeof(sd));
    sd.BufferCount = 1;
    sd.BufferDesc.Width = width;
    sd.BufferDesc.Height = height;
    sd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
    sd.BufferDesc.RefreshRate.Numerator = 60;
    sd.BufferDesc.RefreshRate.Denominator = 1;
    sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
    sd.OutputWindow = hwnd;
    sd.SampleDesc.Count = 1;
    sd.SampleDesc.Quality = 0;
    sd.Windowed = TRUE;

    hr = dxgi_factory->CreateSwapChain(m_device.get(), &sd, &swapchain);
  }

  // we don't handle full-screen swapchains so we block the ALT+ENTER shortcut
  dxgi_factory->MakeWindowAssociation(hwnd, DXGI_MWA_NO_ALT_ENTER);
  dxgi_factory->Release();

  if (!swapchain) 
  {
    return;
  }

  ID3D11Texture2D* back_buffer = nullptr;
  hr = swapchain->GetBuffer(0, __uuidof(ID3D11Texture2D), reinterpret_cast<void**>(&back_buffer));
  if (FAILED(hr))
  {
    swapchain->Release();
    return;
  }

  ID3D11RenderTargetView* rtv = nullptr;
  hr = m_device->CreateRenderTargetView(back_buffer, nullptr, &rtv);
  back_buffer->Release();
  if (FAILED(hr))
  {
    swapchain->Release();
    return;
  }

  auto const ctx = (ID3D11DeviceContext*)(m_context.get());

  ctx->OMSetRenderTargets(1, &rtv, nullptr);

  // Setup the viewport
  D3D11_VIEWPORT vp;
  vp.Width = (FLOAT)width;
  vp.Height = (FLOAT)height;
  vp.MinDepth = 0.0f;
  vp.MaxDepth = 1.0f;
  vp.TopLeftX = 0;
  vp.TopLeftY = 0;
  ctx->RSSetViewports(1, &vp);

  // create a default sampler to use
  ID3D11SamplerState* sampler = nullptr;
  {
    D3D11_SAMPLER_DESC desc = {};
    desc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
    desc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
    desc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
    desc.ComparisonFunc = D3D11_COMPARISON_NEVER;
    desc.MinLOD = 0.0f;
    desc.MaxLOD = D3D11_FLOAT32_MAX;
    desc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
    m_device->CreateSamplerState(&desc, &sampler);
  }

  // create a default blend state to use (pre-multiplied alpha)
  ID3D11BlendState* blender = nullptr;
  {
    D3D11_BLEND_DESC desc;
    desc.AlphaToCoverageEnable = FALSE;
    desc.IndependentBlendEnable = FALSE;
    auto const count = sizeof(desc.RenderTarget) / sizeof(desc.RenderTarget[0]);
    for (uint32_t n = 0; n < count; ++n)
    {
      desc.RenderTarget[n].BlendEnable = TRUE;
      desc.RenderTarget[n].SrcBlend = D3D11_BLEND_ONE;
      desc.RenderTarget[n].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
      desc.RenderTarget[n].SrcBlendAlpha = D3D11_BLEND_ONE;
      desc.RenderTarget[n].DestBlendAlpha = D3D11_BLEND_INV_SRC_ALPHA;
      desc.RenderTarget[n].BlendOp = D3D11_BLEND_OP_ADD;
      desc.RenderTarget[n].BlendOpAlpha = D3D11_BLEND_OP_ADD;
      desc.RenderTarget[n].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
    }
    m_device->CreateBlendState(&desc, &blender);
  }

  m_swapChain = make_shared<SwapChain>(swapchain, rtv, sampler, blender);
}

void Device::PreSwapBuffer()
{
  if (m_swapChain)
  {
    m_swapChain->Bind(shared_from_this());
    m_swapChain->Clear(1.0f, 1.0f, 1.0f, 1.0f);
  }
}

void Device::SwapBuffer()
{
  if (m_swapChain)
  {
    m_swapChain->SwapBuffer();
  }
}

shared_ptr<IGeometry> Device::CreateQuad(float x, float y, float width, float height, bool flip)
{
  x = (x * 2.0f) - 1.0f;
  y = 1.0f - (y * 2.0f);
  width = width * 2.0f;
  height = height * 2.0f;
  float z = 1.0f;

  SimpleVertex vertices[] = {

    { DirectX::XMFLOAT3(x, y, z), DirectX::XMFLOAT2(0.0f, 0.0f) },
    { DirectX::XMFLOAT3(x + width, y, z), DirectX::XMFLOAT2(1.0f, 0.0f) },
    { DirectX::XMFLOAT3(x, y - height, z), DirectX::XMFLOAT2(0.0f, 1.0f) },
    { DirectX::XMFLOAT3(x + width, y - height, z), DirectX::XMFLOAT2(1.0f, 1.0f) }
  };

  if (flip)
  {
    DirectX::XMFLOAT2 tmp(vertices[2].tex);
    vertices[2].tex = vertices[0].tex;
    vertices[0].tex = tmp;

    tmp = vertices[3].tex;
    vertices[3].tex = vertices[1].tex;
    vertices[1].tex = tmp;
  }

  D3D11_BUFFER_DESC desc = {};
  desc.Usage = D3D11_USAGE_DEFAULT;
  desc.ByteWidth = sizeof(SimpleVertex) * 4;
  desc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
  desc.CPUAccessFlags = 0;

  D3D11_SUBRESOURCE_DATA srd = {};
  srd.pSysMem = vertices;

  ID3D11Buffer* buffer = nullptr;
  auto const hr = m_device->CreateBuffer(&desc, &srd, &buffer);
  if (SUCCEEDED(hr))
  {
    return make_shared<Geometry>(
      D3D_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP, 4, static_cast<uint32_t>(sizeof(SimpleVertex)), buffer);
  }

  return nullptr;
}

shared_ptr<ITexture2D> Device::OpenSharedTexture(void* handle)
{
  ID3D11Texture2D* tex = nullptr;
  auto hr = m_device->OpenSharedResource(handle, __uuidof(ID3D11Texture2D), (void**)(&tex));
  if (FAILED(hr))
  {
    return nullptr;
  }

  D3D11_TEXTURE2D_DESC td;
  tex->GetDesc(&td);

  ID3D11ShaderResourceView* srv = nullptr;

  if (td.BindFlags & D3D11_BIND_SHADER_RESOURCE)
  {
    D3D11_SHADER_RESOURCE_VIEW_DESC srv_desc;
    srv_desc.Format = td.Format;
    srv_desc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
    srv_desc.Texture2D.MostDetailedMip = 0;
    srv_desc.Texture2D.MipLevels = 1;

    hr = m_device->CreateShaderResourceView(tex, &srv_desc, &srv);
    if (FAILED(hr))
    {
      tex->Release();
      return nullptr;
    }
  }

  return make_shared<Texture2D>(tex, srv);
}

shared_ptr<ITexture2D> Device::CreateTexture(int width, int height, int format, const void* data, uint32_t row_stride)
{
  D3D11_TEXTURE2D_DESC td;
  td.ArraySize = 1;
  td.BindFlags = D3D11_BIND_SHADER_RESOURCE;
  td.CPUAccessFlags = data ? 0 : D3D11_CPU_ACCESS_WRITE;
  td.Format = (DXGI_FORMAT)format;
  td.Width = width;
  td.Height = height;
  td.MipLevels = 1;
  td.MiscFlags = 0;
  td.SampleDesc.Count = 1;
  td.SampleDesc.Quality = 0;
  td.Usage = data ? D3D11_USAGE_DEFAULT : D3D11_USAGE_DYNAMIC;

  D3D11_SUBRESOURCE_DATA srd;
  srd.pSysMem = data;
  srd.SysMemPitch = static_cast<uint32_t>(row_stride);
  srd.SysMemSlicePitch = 0;

  ID3D11Texture2D* tex = nullptr;
  auto hr = m_device->CreateTexture2D(&td, data ? &srd : nullptr, &tex);
  if (FAILED(hr))
  {
    return nullptr;
  }

  D3D11_SHADER_RESOURCE_VIEW_DESC srv_desc;
  srv_desc.Format = td.Format;
  srv_desc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
  srv_desc.Texture2D.MostDetailedMip = 0;
  srv_desc.Texture2D.MipLevels = 1;

  ID3D11ShaderResourceView* srv = nullptr;
  hr = m_device->CreateShaderResourceView(tex, &srv_desc, &srv);
  if (FAILED(hr))
  {
    tex->Release();
    return nullptr;
  }

  return make_shared<Texture2D>(tex, srv);
}


void Device::BindTexture2D(const std::shared_ptr<ITexture2D>& texture)
{

}

shared_ptr<ID3DBlob> Device::CompileShader(
  string const& source_code,
  string const& entry_point,
  string const& model)
{
  if (!m_libCompiler) {
    return nullptr;
  }

  typedef HRESULT(WINAPI* PFN_D3DCOMPILE)(
    LPCVOID, SIZE_T, LPCSTR, const D3D_SHADER_MACRO*,
    ID3DInclude*, LPCSTR, LPCSTR, UINT, UINT, ID3DBlob**, ID3DBlob**);

  auto const fnc_compile = reinterpret_cast<PFN_D3DCOMPILE>(
    GetProcAddress(m_libCompiler, "D3DCompile"));
  if (!fnc_compile)
  {
    return nullptr;
  }

  DWORD flags = D3DCOMPILE_ENABLE_STRICTNESS;

#if defined(NDEBUG)
  //flags |= D3DCOMPILE_OPTIMIZATION_LEVEL3;
  //flags |= D3DCOMPILE_AVOID_FLOW_CONTROL;
#else
  flags |= D3DCOMPILE_DEBUG;
  flags |= D3DCOMPILE_SKIP_OPTIMIZATION;
#endif

  ID3DBlob* blob = nullptr;
  ID3DBlob* blob_err = nullptr;

  auto const psrc = source_code.c_str();
  auto const len = source_code.size() + 1;

  auto const hr = fnc_compile(
    psrc, len, nullptr, nullptr, nullptr,
    entry_point.c_str(),
    model.c_str(),
    flags,
    0,
    &blob,
    &blob_err);

  if (FAILED(hr))
  {
    if (blob_err)
    {
      // TODO: log the error
      blob_err->Release();
    }
    return nullptr;
  }

  if (blob_err)
  {
    blob_err->Release();
  }

  return shared_ptr<ID3DBlob>(blob, [](ID3DBlob* p) { if (p) p->Release(); });
}


//
// create some basic shaders so we can draw a textured-quad
//
shared_ptr<Effect> Device::CreateDefaultEffect()
{
  auto const vsh =
    R"--(struct VS_INPUT
{
	float4 pos : POSITION;
	float2 tex : TEXCOORD0;
};

struct VS_OUTPUT
{
	float4 pos : SV_POSITION;
	float2 tex : TEXCOORD0;
};

VS_OUTPUT main(VS_INPUT input)
{
	VS_OUTPUT output;
	output.pos = input.pos;
	output.tex = input.tex;
	return output;
})--";

  auto const psh =
    R"--(Texture2D tex0 : register(t0);
SamplerState samp0 : register(s0);

struct VS_OUTPUT
{
	float4 pos : SV_POSITION;
	float2 tex : TEXCOORD0;
};

float4 main(VS_OUTPUT input) : SV_Target
{
	return tex0.Sample(samp0, input.tex);
})--";

  return CreateEffect(
    vsh,
    "main",
    "vs_4_0",
    psh,
    "main",
    "ps_4_0");
}

shared_ptr<Effect> Device::CreateEffect(
  string const& vertex_code,
  string const& vertex_entry,
  string const& vertex_model,
  string const& pixel_code,
  string const& pixel_entry,
  string const& pixel_model)
{
  auto const vs_blob = CompileShader(vertex_code, vertex_entry, vertex_model);

  ID3D11VertexShader* vshdr = nullptr;
  ID3D11InputLayout* layout = nullptr;

  if (vs_blob)
  {
    m_device->CreateVertexShader(
      vs_blob->GetBufferPointer(),
      vs_blob->GetBufferSize(),
      nullptr,
      &vshdr);

    D3D11_INPUT_ELEMENT_DESC layout_desc[] =
    {
      { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
      { "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
    };

    UINT elements = ARRAYSIZE(layout_desc);

    // Create the input layout
    m_device->CreateInputLayout(
      layout_desc,
      elements,
      vs_blob->GetBufferPointer(),
      vs_blob->GetBufferSize(),
      &layout);
  }

  auto const ps_blob = CompileShader(pixel_code, pixel_entry, pixel_model);
  ID3D11PixelShader* pshdr = nullptr;
  if (ps_blob)
  {
    m_device->CreatePixelShader(
      ps_blob->GetBufferPointer(),
      ps_blob->GetBufferSize(),
      nullptr,
      &pshdr);
  }

  return make_shared<Effect>(vshdr, pshdr, layout);
}


shared_ptr<Device> CreateDevice()
{
  UINT flags = 0;
#ifdef _DEBUG
  flags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

  D3D_FEATURE_LEVEL feature_levels[] =
  {
    D3D_FEATURE_LEVEL_11_1,
    D3D_FEATURE_LEVEL_11_0,
    D3D_FEATURE_LEVEL_10_1,
    D3D_FEATURE_LEVEL_10_0,
    //D3D_FEATURE_LEVEL_9_3,
  };
  UINT num_feature_levels = sizeof(feature_levels) / sizeof(feature_levels[0]);


  ID3D11Device* pdev = nullptr;
  ID3D11DeviceContext* pctx = nullptr;

  D3D_FEATURE_LEVEL selected_level;
  HRESULT hr = D3D11CreateDevice(
    nullptr,
    D3D_DRIVER_TYPE_HARDWARE,
    nullptr,
    flags, feature_levels,
    num_feature_levels,
    D3D11_SDK_VERSION,
    &pdev,
    &selected_level,
    &pctx);

  if (hr == E_INVALIDARG)
  {
    // DirectX 11.0 platforms will not recognize D3D_FEATURE_LEVEL_11_1 
    // so we need to retry without it
    hr = D3D11CreateDevice(
      nullptr,
      D3D_DRIVER_TYPE_HARDWARE,
      nullptr,
      flags,
      &feature_levels[1],
      num_feature_levels - 1,
      D3D11_SDK_VERSION,
      &pdev,
      &selected_level,
      &pctx);
  }

  if (SUCCEEDED(hr))
  {
    auto const dev = make_shared<Device>(pdev, pctx);

    LOG(INFO) << "d3d11: selected adapter: " << dev->GetAdaptername().c_str();

    LOG(INFO) << "d3d11: selected feature level: " << selected_level;

    return dev;
  }

  return nullptr;
}

void Device::Render(const std::shared_ptr<ITexture2D>& texture, const std::shared_ptr<IGeometry> &geometry)
{
  if (texture)
  {
    if (!m_effect)
    {
      m_effect = CreateDefaultEffect();
    }

    // bind our states/resource to the pipeline
    ScopedBinder<IGeometry> quad_binder(shared_from_this(), geometry);
    ScopedBinder<Effect> fx_binder(shared_from_this(), m_effect);
    ScopedBinder<ITexture2D> tex_binder(shared_from_this(), texture);

    // actually draw the quad
    geometry->Draw();
  }
}
