#ifndef RENDER_H
#define RENDER_H

class IDevice;
class ITexture2D
{
public:
  virtual uint32_t Width() const = 0;
  virtual uint32_t Height() const = 0;
  virtual int Format() const = 0;
  virtual void CopyFrom(const void* buffer, uint32_t stride, uint32_t rows) = 0;
  virtual void Bind(const std::shared_ptr<IDevice> &device) = 0;
  virtual void UnBind() = 0;
  virtual void* ShareHandle() const = 0;
};

class IGeometry
{
public:
  virtual void Bind(const std::shared_ptr<IDevice> &device) = 0;
  virtual void UnBind() = 0;
  virtual void Draw() = 0;
};

class IDevice
{
public:
  virtual std::wstring GetAdaptername() = 0;
  virtual void Render(const std::shared_ptr<ITexture2D>& texture, const std::shared_ptr<IGeometry> &geometry) = 0;
  virtual std::shared_ptr<ITexture2D> CreateTexture(int width, int height, int format, const void* data, uint32_t stride) = 0;
  virtual void BindTexture2D(const std::shared_ptr<ITexture2D>& texture) = 0;
  virtual void* GetContext() = 0;
  virtual void PreSwapBuffer() = 0;
  virtual void SwapBuffer() = 0;
  virtual void CreateSwapBuffer(int width, int height) = 0;
  virtual std::shared_ptr<ITexture2D> OpenSharedTexture(void*) = 0;
  virtual std::shared_ptr<IGeometry> CreateQuad(float x, float y, float width, float height, bool flip = false) = 0;
};

#endif //!RENDER_H