#ifndef LAYER_H
#define LAYER_H

#include <vector>
#include <mutex>
#include "composition.h"
#include "render/render.h"

struct Rect
{
  float x;
  float y;
  float width;
  float height;
};

class Layer
{
public:
  Layer(const std::shared_ptr<IDevice> &device, bool flip);
  ~Layer();

  virtual void Tick(uint64_t t);
  virtual void Attach(const std::shared_ptr<Composition> &);
  virtual void Move(float x, float y, float width, float height);
  virtual void Render(const std::shared_ptr<IDevice> & device) = 0;

  Rect Bounds() const;
  std::shared_ptr<Composition> GetComposition() const;

protected:
  void RenderTexture(const std::shared_ptr<IDevice> &device, const std::shared_ptr<ITexture2D> &texture, const std::shared_ptr<IGeometry> &geometry);
  bool m_flip;
  Rect m_bounds;

  const std::shared_ptr<IDevice> m_device;
  std::shared_ptr<IGeometry> m_geometry;
private:
  std::weak_ptr<Composition> m_composition;
};
#endif //!LAYER_H