#ifndef WEBLAYER_H
#define WEBLAYER_H

#include "render/layer.h"
#include "cef/cef_webview.h"

class IDeivce;
class WebLayer : public Layer
{
public:
  WebLayer(const std::shared_ptr<IDevice> & device, const CefRefPtr<WebView> & view);
  ~WebLayer();

  void Tick(uint64_t t) override;
  void Render(const std::shared_ptr<IDevice> & device) override;

private:
  const CefRefPtr<WebView> m_webview;
};
std::shared_ptr<WebLayer> CreateWebLayer(const std::shared_ptr<IDevice> & device, const std::string& url, int width, int height, int fps = 30);
#endif //WEBLAYER_H