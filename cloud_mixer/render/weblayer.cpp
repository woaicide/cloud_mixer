#include "weblayer.h"

WebLayer::WebLayer(const std::shared_ptr<IDevice> & device, const CefRefPtr<WebView> & view)
  : Layer(device, view->UseSharedTextures())
  , m_webview(view)
{

}

WebLayer::~WebLayer()
{
  if (m_webview)
    m_webview->Close();
}

void WebLayer::Tick(uint64_t t)
{
  const auto comp = GetComposition();
  if (comp)
  {
    const auto rect = Bounds();
    const auto width = static_cast<int>(rect.width * comp->Width());
    const auto height = static_cast<int>(rect.height * comp->Height());

    if (m_webview)
    {
      m_webview->Resize(width, height);
      m_webview->RequestFrame();
    }
  }

  Layer::Tick(t);
}

void WebLayer::Render(const std::shared_ptr<IDevice> & device)
{
  if (!m_geometry)
  {
    m_geometry = device->CreateQuad(m_bounds.x, m_bounds.y, m_bounds.width, m_bounds.height, m_flip);
  }
  if (m_webview)
    RenderTexture(device, m_webview->GetTexture(device), m_geometry);
}

std::shared_ptr<WebLayer> CreateWebLayer(const std::shared_ptr<IDevice> & device, const std::string& url, int width, int height, int fps)
{
  CefWindowInfo window_info;
  window_info.SetAsWindowless(nullptr);
  window_info.shared_texture_enabled = false;
  window_info.external_begin_frame_enabled = false;

  CefBrowserSettings settings;
  settings.windowless_frame_rate = fps;

  CefRefPtr<WebView> view(new WebView(url, device, width, height, window_info.shared_texture_enabled,window_info.external_begin_frame_enabled));
  CefBrowserHost::CreateBrowser(window_info, view, url.c_str(), settings, nullptr);
  return std::make_shared<WebLayer>(device, view);
}
