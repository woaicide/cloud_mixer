#include "layer.h"
#include "render.h"

Layer::Layer(const std::shared_ptr<IDevice> &device, bool flip)
  : m_device(device)
  , m_flip(flip)
{
  m_bounds.x = m_bounds.y = m_bounds.width = m_bounds.height = 0.0f;
}

Layer::~Layer()
{

}

void Layer::Tick(uint64_t t)
{

}

void Layer::Attach(const std::shared_ptr<Composition> &parent)
{
  m_composition = parent;
}

void Layer::Move(float x, float y, float width, float height)
{
  m_bounds.x = x;
  m_bounds.y = y;
  m_bounds.width = width;
  m_bounds.height = height;
  m_geometry.reset();
}

Rect Layer::Bounds() const
{
  return m_bounds;
}

std::shared_ptr<Composition> Layer::GetComposition() const
{
  return m_composition.lock();
}

void Layer::RenderTexture(const std::shared_ptr<IDevice> &device, const std::shared_ptr<ITexture2D> &texture, const std::shared_ptr<IGeometry> &geometry)
{
  if (device && texture)
  {
    device->Render(texture, geometry);
  }
}

