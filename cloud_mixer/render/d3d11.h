#ifndef D3D11_H
#define D3D11_H

#include <d3d11_1.h>
#include <memory>
#include <string>
#include "platform.h"
#include "render/render.h"

class SwapChain;
class Geometry;
class Effect;
class Texture2D;
class Context;

template<class T>
class ScopedBinder
{
public:
  ScopedBinder(const std::shared_ptr<IDevice> & device, const std::shared_ptr<T> & target)
    : m_target(target)
  {
    if (m_target) { m_target->Bind(device); }
  }
  ~ScopedBinder()
  { 
    if (m_target) 
    { 
      m_target->UnBind();
    } 
  }
private:
  const std::shared_ptr<T> m_target;
};

class Device : public IDevice, public std::enable_shared_from_this<Device>
{
public:
  Device(ID3D11Device*, ID3D11DeviceContext*);
  ~Device();

  std::wstring GetAdaptername() override;
  std::shared_ptr<ITexture2D> CreateTexture(int width, int height, int format, const void* data, uint32_t row_stride) override;
  void BindTexture2D(const std::shared_ptr<ITexture2D>& texture) override;
  void Render(const std::shared_ptr<ITexture2D>& texture, const std::shared_ptr<IGeometry> &geometry) override;
  void* GetContext() override;
  void CreateSwapBuffer(int width, int height) override;
  std::shared_ptr<ITexture2D> OpenSharedTexture(void*) override;
  std::shared_ptr<IGeometry> CreateQuad(float x, float y, float width, float height, bool flip = false) override;

  //swapbuffer
  void PreSwapBuffer() override;
  void SwapBuffer() override;

private:
  std::shared_ptr<Effect> CreateDefaultEffect();
  std::shared_ptr<Effect> CreateEffect(
    std::string const& vertex_code,
    std::string const& vertex_entry,
    std::string const& vertex_model,
    std::string const& pixel_code,
    std::string const& pixel_entry,
    std::string const& pixel_model);

private:
  std::shared_ptr<ID3DBlob> CompileShader(
    std::string const& source_code,
    std::string const& entry_point,
    std::string const& model);

  HMODULE m_libCompiler;
  const std::shared_ptr<ID3D11Device> m_device;
  const std::shared_ptr<ID3D11DeviceContext> m_context;
  std::shared_ptr<Effect> m_effect;
  std::shared_ptr<SwapChain> m_swapChain = nullptr;
};

class SwapChain
{
public:
  SwapChain(IDXGISwapChain*, ID3D11RenderTargetView*, ID3D11SamplerState*, ID3D11BlendState*);
  ~SwapChain();

  void Bind(std::shared_ptr<IDevice> const& device);
  void UnBind();
  void SwapBuffer();
  void Clear(float red, float green, float blue, float alpha);
  void Resize(int width, int height); 

private:
  void Present(int sync_interval);

private:
  const std::shared_ptr<ID3D11SamplerState> m_sampler;
  const std::shared_ptr<ID3D11BlendState> m_blender;
  const std::shared_ptr<IDXGISwapChain> m_swapchain;
  std::shared_ptr<ID3D11RenderTargetView> m_rtv;
  std::weak_ptr<IDevice> m_device;
};

class Texture2D : public ITexture2D
{
public:
  Texture2D(ID3D11Texture2D* tex, ID3D11ShaderResourceView* srv);
  ~Texture2D();

  void Bind(const std::shared_ptr<IDevice>& device) override;
  void UnBind() override;

  uint32_t Width() const override;
  uint32_t Height() const override;
  int Format() const override;
  void CopyFrom(const void* buffer, uint32_t stride, uint32_t rows) override;
  void* ShareHandle() const override;

private:
  void CopyFrom(std::shared_ptr<Texture2D> const&);

private:
  HANDLE m_shareHandle;
  const std::shared_ptr<ID3D11Texture2D> m_texture;
  const std::shared_ptr<ID3D11ShaderResourceView> m_shaderResourceView;
  std::weak_ptr<IDevice> m_device;
};

class Effect
{
public:
  Effect(ID3D11VertexShader* vsh, ID3D11PixelShader* psh, ID3D11InputLayout* layout);

  void Bind(const std::shared_ptr<IDevice>& device);
  void UnBind();

private:
  const std::shared_ptr<ID3D11VertexShader> m_vertexShader;
  const std::shared_ptr<ID3D11PixelShader> m_pixelShader;
  const std::shared_ptr<ID3D11InputLayout> m_layout;
  std::weak_ptr<IDevice> m_device;
};


class Geometry : public IGeometry
{
public:
  Geometry(D3D_PRIMITIVE_TOPOLOGY primitive, uint32_t vertices, uint32_t stride, ID3D11Buffer*);
  ~Geometry();

  void Bind(const std::shared_ptr<IDevice>& device) override;
  void UnBind() override;
  void Draw() override;

private:
  D3D_PRIMITIVE_TOPOLOGY m_primitive;
  uint32_t m_vertices;
  uint32_t m_stride;
  const std::shared_ptr<ID3D11Buffer> m_buffer;
  std::weak_ptr<IDevice> m_device;
};

std::shared_ptr<Device> CreateDevice();
#endif //D3D11_H