﻿#include "appmanager.h"
//------------------------------------------------------------------------

int main(int argc, char** argv)
{
  if (AppManager::Instance().Init() == NO_ERROR)
  {
    AppManager::Instance().RunLoop();
  }
}