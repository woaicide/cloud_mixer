#ifndef COMPOSITION_H
#define COMPOSITION_H

#include <vector>
#include <mutex>
#include <memory>
#include "render/render.h"

class Layer;
class Composition : public std::enable_shared_from_this<Composition>
{
public:
  Composition(const std::shared_ptr<IDevice> & device, int width, int height);
  ~Composition();

  int Width() const;
  int Height() const;

  void Tick(uint64_t t);
  void Render(const std::shared_ptr<IDevice> &);

  void AddLayer(const std::shared_ptr<Layer> & layer);
  bool RemoveLayer(const std::shared_ptr<Layer> & layer);
  void Resize(int width, int height);
  std::shared_ptr<IDevice> GetDevice();

private:
  int m_width;
  int m_height;
  const std::shared_ptr<IDevice> m_device;
  std::vector<std::shared_ptr<Layer>> m_layers;
  std::mutex m_lock;
}; 
std::shared_ptr<Composition> CreateComposition(int width, int height);
#endif //COMPOSITION_H