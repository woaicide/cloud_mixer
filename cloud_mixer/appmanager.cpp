#include "appmanager.h"
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <string> 
#include <strsafe.h>
#include <fstream>
#include "base/files/file_path.h"
#include "base/files/file_util.h"
#include "base/command_line.h"
#include "base/base_paths.h"
#include "base/path_service.h"
#include "base/threading/platform_thread.h"
#include "cef/cef_moudle.h"
#include "include/cef_parser.h"
#include "render/weblayer.h"

AppManager::AppManager()
{

}

AppManager::~AppManager()
{
  
}

AppManager& AppManager::Instance()
{
  static AppManager ins;
  return ins;
}

void AppManager::IdleLoopCb(uv_idle_t* handle)
{
  if (handle->data)
  {
    static_cast<AppManager *>(handle->data)->IdelLoop(handle);
  }
}

int PrepareWorkDirctory()
{
  base::FilePath module_path;
  if (!base::PathService::Get(base::FILE_EXE, &module_path))
    return -1;

  base::SetCurrentDirectory(module_path.DirName());
  return 0;
}

void InitLogger()
{
  // Initialize logging.
  base::FilePath exe;
  base::PathService::Get(base::FILE_MODULE, &exe);

  base::CreateDirectoryAndGetError(exe.DirName().AppendASCII("log"), nullptr);

  std::ostringstream s;
  s << "log\\" << exe.BaseName().RemoveExtension().AsUTF8Unsafe() << '_';
#if defined(OS_WIN)
  SYSTEMTIME local_time;
  GetLocalTime(&local_time);
  s << std::setfill('0')
    << std::setw(2) << local_time.wMonth
    << std::setw(2) << local_time.wDay
    << '_'
    << std::setw(2) << local_time.wHour
    << std::setw(2) << local_time.wMinute
    << std::setw(2) << local_time.wSecond;
#elif defined(OS_POSIX) || defined(OS_FUCHSIA)
  timeval tv;
  gettimeofday(&tv, nullptr);
  time_t t = tv.tv_sec;
  struct tm local_time;
  localtime_r(&t, &local_time);
  struct tm* tm_time = &local_time;
  s << std::setfill('0')
    << std::setw(2) << 1 + tm_time->tm_mon
    << std::setw(2) << tm_time->tm_mday
    << '_'
    << std::setw(2) << tm_time->tm_hour
    << std::setw(2) << tm_time->tm_min
    << std::setw(2) << tm_time->tm_sec;
#else
#error Unsupported platform
#endif

  s << ".log";
  base::FilePath log_filename = base::FilePath::FromUTF8Unsafe(s.str());
  logging::LoggingSettings settings;
  settings.logging_dest = logging::LOG_TO_ALL;
  settings.log_file = log_filename.value().c_str();
  settings.delete_old = logging::DELETE_OLD_LOG_FILE;
  logging::InitLogging(settings);
  // We want process and thread IDs because we may have multiple processes.
  // Note: temporarily enabled timestamps in an effort to catch bug 6361.
  logging::SetLogItems(true, true, true, true);
}


int AppManager::Init()
{
  PrepareWorkDirctory();
  if (!base::CommandLine::Init(0, 0))
  {
    LOG(ERROR) << "main: command line init failed";
    return -1;
  }

  InitLogger();
#if OS_WIN  
  base::Time::EnableHighResolutionTimer(true);
#endif // OS_WIN

  int exitCode = CefInitialize(GetModuleHandle(NULL));
  if (exitCode >= 0)
  {
    return exitCode;
  }

  uv_loop_init(&m_loop);
  uv_idle_init(&m_loop, &m_idler);
  m_idler.data = (void *)this;
  uv_idle_start(&m_idler, IdleLoopCb);

  int error = DoInitComposition();
  return error;
}

void AppManager::RunLoop()
{
  uv_run(&m_loop, UV_RUN_DEFAULT);
  CefUninitialize();
}

void AppManager::Close()
{
  m_exitLoop = true;
  uv_loop_close(&m_loop);
}

void AppManager::IdelLoop(uv_idle_t* handle)
{
  const static base::TimeDelta timeDelta = base::TimeDelta::FromMilliseconds(1);
  if (!m_exitLoop)
  {
    base::PlatformThread::Sleep(timeDelta);
    DoRenderLoop();
    return;
  }

  uv_idle_stop(handle);
}

int AppManager::DoInitComposition()
{
  base::FilePath exe;
  base::PathService::Get(base::FILE_MODULE, &exe);
  std::string filepath = exe.DirName().AppendASCII("cloud_mixer.json").AsUTF8Unsafe();
  std::ifstream file(filepath, std::ios::in | std::ios::ate);
  int size = file.tellg();
  std::shared_ptr<uint8_t> buffer = std::shared_ptr<uint8_t>((uint8_t*)malloc(size), free);
  file.seekg(0, std::ios::beg);
  file.read((char *)buffer.get(), size);
  std::string jsonStr((char *)buffer.get(), size);
  file.close();
  const auto jsonVal = CefParseJSON(jsonStr.c_str(), JSON_PARSER_ALLOW_TRAILING_COMMAS);
  if (jsonVal == NULL)
    return -2;
  const auto dict = jsonVal->GetDictionary();
  int width = dict->GetInt("width");
  int height = dict->GetInt("height");
  int fps = dict->GetInt("fps");
  m_loopMillSec = 1000 / fps;
  m_composition = CreateComposition(width, height);
  if (!m_composition)
    return -3;
  const auto layers = dict->GetList("layers");
  if (layers && layers->IsValid())
  {
    for (uint32_t i = 0; i < layers->GetSize(); ++i)
    {
      if (layers->GetType(i) == VTYPE_DICTIONARY)
      {
        const auto obj = layers->GetDictionary(i);
        if (obj && obj->IsValid())
        {
          std::string type = obj->GetString("type");
          std::string src = obj->GetString("src");
          std::shared_ptr<Layer> layer;
          if (type == "web")
          {
            layer = CreateWebLayer(m_composition->GetDevice(), src, width, height, fps);
          }
          if (!layer)
            continue;
          m_composition->AddLayer(layer);

          auto x = obj->GetDouble("left");
          auto y = obj->GetDouble("top");
          auto w = obj->GetDouble("width");
          auto h = obj->GetDouble("height");
          layer->Move(x, y, w, h);
        }
      }
    }
  }

  return 0;
}

void AppManager::DoRenderLoop()
{
  base::TimeTicks current = base::TimeTicks::Now();
  base::TimeDelta fpsInterval = base::TimeDelta::FromMilliseconds(m_loopMillSec);
  if ((current - m_lastTick) > fpsInterval)
  {
    m_composition->Tick(current.ToInternalValue());
    m_lastTick = current;
  }
}
