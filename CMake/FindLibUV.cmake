# - Try to find Glog
#
# The following variables are optionally searched for defaults
#  LIBUV_ROOT_DIR:            Base directory where all GLOG components are found
#
# The following are set after configuration is done: 
#  LIBUV_FOUND
#  LIBUV_INCLUDE_DIRS
#  LIBUV_LIBRARIES

include(FindPackageHandleStandardArgs)
set(LIBUV_ROOT_DIR ${THIRDPARTY_PATH}/libuv-1.29.1)

if(WIN32)
  set(WINDOWS_PLATFORM win64)
  if (NOT "${CMAKE_GENERATOR_PLATFORM}" MATCHES "(Win64|IA64|x64)")
    set(WINDOWS_PLATFORM win32)
  endif()
else()
  #set(LIBUV_ROOT_DIR /usr/local)
endif()

if(WIN32)
  find_path(LIBUV_INCLUDE_DIR uv.h
    PATHS ${LIBUV_ROOT_DIR}/include)
else()
  find_path(LIBUV_INCLUDE_DIR uv.h
    HINTS ${LIBUV_ROOT_DIR}/include)
endif()

if(MSVC)
  find_library(LIBUV_LIBRARY_RELEASE uv.lib PATHS ${LIBUV_ROOT_DIR}/lib/${WINDOWS_PLATFORM}/Release)
  find_library(LIBUV_LIBRARY_DEBUG uv.lib PATHS ${LIBUV_ROOT_DIR}/lib/${WINDOWS_PLATFORM}/Debug)
  set(LIBUV_LIBRARY optimized ${LIBUV_LIBRARY_RELEASE} debug ${LIBUV_LIBRARY_DEBUG})
else()
  find_library(LIBUV_LIBRARY uv HINTS ${LIBUV_ROOT_DIR}/lib/linux)
endif()

find_package_handle_standard_args(LIBUV DEFAULT_MSG
  LIBUV_INCLUDE_DIR LIBUV_LIBRARY)

if(LIBUV_FOUND)
  set(LIBUV_INCLUDE_DIRS ${LIBUV_INCLUDE_DIR})
  set(LIBUV_LIBRARIES ${LIBUV_LIBRARY})
endif()

if(WIN32)
  list(APPEND LIBUV_LIBRARIES iphlpapi)
  list(APPEND LIBUV_LIBRARIES psapi)
  list(APPEND LIBUV_LIBRARIES userenv)
  list(APPEND LIBUV_LIBRARIES ws2_32)
  list(APPEND LIBUV_LIBRARIES shlwapi)
endif()

message("LIBUV_FOUND= ${LIBUV_FOUND}")

