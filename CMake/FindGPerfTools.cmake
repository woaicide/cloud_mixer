# - Try to find GPERFTOOLS
#
# The following variables are optionally searched for defaults
#  GPERFTOOLS_ROOT_DIR:            Base directory where all GPERFTOOLS components are found
#
# The following are set after configuration is done: 
#  GPERFTOOLS_FOUND
#  GPERFTOOLS_INCLUDE_DIRS
#  GPERFTOOLS_LIBRARIES

include(FindPackageHandleStandardArgs)
set(GPERFTOOLS_ROOT_DIR ${THIRDPARTY_PATH}/gperftools-2.7)

if(WIN32)
  set(WINDOWS_PLATFORM win64)
  if (NOT "${CMAKE_GENERATOR_PLATFORM}" MATCHES "(Win64|IA64|x64)")
    set(WINDOWS_PLATFORM win32)
  endif()
else()
  #set(GPERFTOOLS_ROOT_DIR /usr/local)
endif()

if(WIN32)
  find_path(GPERFTOOLS_INCLUDE_DIR tcmalloc.h
    PATHS ${GPERFTOOLS_ROOT_DIR}/src/windows/gperftools)
else()
  find_path(GPERFTOOLS_INCLUDE_DIR tcmalloc.h
    HINTS ${GPERFTOOLS_ROOT_DIR}/src/gperftools)
endif()

if(MSVC)
  find_library(TCMALLOC_LIBRARY_RELEASE libtcmalloc_minimal.lib PATHS ${GPERFTOOLS_ROOT_DIR}/lib/${WINDOWS_PLATFORM}/Release)
  find_library(TCMALLOC_LIBRARY_DEBUG libtcmalloc_minimal.lib PATHS ${GPERFTOOLS_ROOT_DIR}/lib/${WINDOWS_PLATFORM}/Debug)
  set(TCMALLOC_LIBRARY optimized ${TCMALLOC_LIBRARY_RELEASE} debug ${TCMALLOC_LIBRARY_DEBUG})
else()
  find_library(TCMALLOC_LIBRARY tcmalloc_minimal PATHS ${GPERFTOOLS_ROOT_DIR}/lib/linux)
endif()

find_package_handle_standard_args(GPERFTOOLS DEFAULT_MSG GPERFTOOLS_INCLUDE_DIR TCMALLOC_LIBRARY)

if(GPERFTOOLS_FOUND)
  set(GPERFTOOLS_INCLUDE_DIRS ${GPERFTOOLS_INCLUDE_DIR})
  set(TCMALLOC_LIBRARIES ${TCMALLOC_LIBRARY})
endif()

message("GPERFTOOLS_FOUND= ${GPERFTOOLS_FOUND}")