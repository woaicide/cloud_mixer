# - Try to find HY_VIDEO
#
# The following variables are optionally searched for defaults
#  CEF_ROOT_DIR:            Base directory where all CEF components are found
#
# The following are set after configuration is done: 
#  CEF_FOUND
#  CEF_LIBRARIES
#  CEFBIN_LIBRARY
#  CEF_INCLUDE_DIRS

include(FindPackageHandleStandardArgs)

set(CEF_ROOT_DIR ${THIRDPARTY_PATH}/cef)

if(WIN32)
  set(PLATFORM win64)
  if (NOT "${CMAKE_GENERATOR_PLATFORM}" MATCHES "(Win64|IA64|x64)")
    set(PLATFORM win32)
  endif()
else()
  set(PLATFORM linux)
endif()

if(WIN32)
  find_library(CEF_LIBRARIES libcef.lib PATHS ${CEF_ROOT_DIR}/lib/${PLATFORM})
  set(CEFBIN_LIBRARY ${CEF_ROOT_DIR}/bin/${PLATFORM})
else()
  find_library(CEF_LIBRARIES libcef PATHS ${CEF_ROOT_DIR}/lib/${PLATFORM})
  set(CEFBIN_LIBRARY ${CEF_ROOT_DIR}/bin/${PLATFORM})
endif()

find_package_handle_standard_args(CEF DEFAULT_MSG
  CEF_LIBRARIES CEFBIN_LIBRARY)

if(CEF_FOUND)
  set(CEF_LIBRARIES ${CEF_LIBRARIES})
  set(CEFBIN_LIBRARY ${CEFBIN_LIBRARY})
  set(CEF_INCLUDE_DIRS ${CEF_ROOT_DIR})
endif()

message("CEF_FOUND= ${CEF_FOUND}")

