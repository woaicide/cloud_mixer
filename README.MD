# CEF Offscreen-Rendering (OSR) Demo

This application uses D3D11 shared textures for CEF which improves the OSR rendering performance.
todo implemented by GL

## Build Instructions

1. If you don't have it already - install CMake and Visual Studio 2017
    * VS 2017 Community Edition is fine - just make sure to install C/C++ development tools

2. mkdir build32
   cd build32
   cmake -G "Visual Studio 15 2017" ..
3. open mixer.sln and build it.