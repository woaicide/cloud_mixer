# Copyright (c) 2014 The Chromium Embedded Framework Authors. All rights
# reserved. Use of this source code is governed by a BSD-style license that
# can be found in the LICENSE file.

# Append platform specific sources to a list of sources.
macro(LIBCEF_APPEND_PLATFORM_SOURCES name_of_list)
  if("${CMAKE_SYSTEM_NAME}" STREQUAL "Darwin" AND ${name_of_list}_MACOSX)
    list(APPEND ${name_of_list} ${${name_of_list}_MACOSX})
  endif()
  if("${CMAKE_SYSTEM_NAME}" STREQUAL "Linux" AND ${name_of_list}_LINUX)
    list(APPEND ${name_of_list} ${${name_of_list}_LINUX})
  endif()
  if("${CMAKE_SYSTEM_NAME}" STREQUAL "Windows" AND ${name_of_list}_WINDOWS)
    list(APPEND ${name_of_list} ${${name_of_list}_WINDOWS})
  endif()
endmacro()

set(CEF_TARGET libcef_dll_wrapper)

set(LIBCEF_SRCS
  ptr_util.h
  transfer_util.cc
  transfer_util.h
  wrapper_types.h
  )
source_group(libcef_dll FILES ${LIBCEF_SRCS})

set(LIBCEF_BASE_SRCS
  base/cef_atomicops_x86_gcc.cc
  base/cef_bind_helpers.cc
  base/cef_callback_helpers.cc
  base/cef_callback_internal.cc
  base/cef_lock.cc
  base/cef_lock_impl.cc
  base/cef_logging.cc
  base/cef_ref_counted.cc
  base/cef_string16.cc
  base/cef_thread_checker_impl.cc
  base/cef_weak_ptr.cc
  )
source_group(libcef_dll\\\\base FILES ${LIBCEF_BASE_SRCS})

set(LIBCEF_CPPTOC_SRCS
  cpptoc/accessibility_handler_cpptoc.cc
  cpptoc/accessibility_handler_cpptoc.h
  cpptoc/app_cpptoc.cc
  cpptoc/app_cpptoc.h
  cpptoc/base_ref_counted_cpptoc.cc
  cpptoc/base_ref_counted_cpptoc.h
  cpptoc/base_scoped_cpptoc.cc
  cpptoc/base_scoped_cpptoc.h
  cpptoc/browser_process_handler_cpptoc.cc
  cpptoc/browser_process_handler_cpptoc.h
  cpptoc/client_cpptoc.cc
  cpptoc/client_cpptoc.h
  cpptoc/completion_callback_cpptoc.cc
  cpptoc/completion_callback_cpptoc.h
  cpptoc/context_menu_handler_cpptoc.cc
  cpptoc/context_menu_handler_cpptoc.h
  cpptoc/cookie_visitor_cpptoc.cc
  cpptoc/cookie_visitor_cpptoc.h
  cpptoc/cpptoc_ref_counted.h
  cpptoc/cpptoc_scoped.h
  cpptoc/delete_cookies_callback_cpptoc.cc
  cpptoc/delete_cookies_callback_cpptoc.h
  cpptoc/dialog_handler_cpptoc.cc
  cpptoc/dialog_handler_cpptoc.h
  cpptoc/display_handler_cpptoc.cc
  cpptoc/display_handler_cpptoc.h
  cpptoc/domvisitor_cpptoc.cc
  cpptoc/domvisitor_cpptoc.h
  cpptoc/download_handler_cpptoc.cc
  cpptoc/download_handler_cpptoc.h
  cpptoc/download_image_callback_cpptoc.cc
  cpptoc/download_image_callback_cpptoc.h
  cpptoc/drag_handler_cpptoc.cc
  cpptoc/drag_handler_cpptoc.h
  cpptoc/end_tracing_callback_cpptoc.cc
  cpptoc/end_tracing_callback_cpptoc.h
  cpptoc/extension_handler_cpptoc.cc
  cpptoc/extension_handler_cpptoc.h
  cpptoc/find_handler_cpptoc.cc
  cpptoc/find_handler_cpptoc.h
  cpptoc/focus_handler_cpptoc.cc
  cpptoc/focus_handler_cpptoc.h
  cpptoc/jsdialog_handler_cpptoc.cc
  cpptoc/jsdialog_handler_cpptoc.h
  cpptoc/keyboard_handler_cpptoc.cc
  cpptoc/keyboard_handler_cpptoc.h
  cpptoc/life_span_handler_cpptoc.cc
  cpptoc/life_span_handler_cpptoc.h
  cpptoc/load_handler_cpptoc.cc
  cpptoc/load_handler_cpptoc.h
  cpptoc/menu_model_delegate_cpptoc.cc
  cpptoc/menu_model_delegate_cpptoc.h
  cpptoc/navigation_entry_visitor_cpptoc.cc
  cpptoc/navigation_entry_visitor_cpptoc.h
  cpptoc/pdf_print_callback_cpptoc.cc
  cpptoc/pdf_print_callback_cpptoc.h
  cpptoc/print_handler_cpptoc.cc
  cpptoc/print_handler_cpptoc.h
  cpptoc/read_handler_cpptoc.cc
  cpptoc/read_handler_cpptoc.h
  cpptoc/register_cdm_callback_cpptoc.cc
  cpptoc/register_cdm_callback_cpptoc.h
  cpptoc/render_handler_cpptoc.cc
  cpptoc/render_handler_cpptoc.h
  cpptoc/render_process_handler_cpptoc.cc
  cpptoc/render_process_handler_cpptoc.h
  cpptoc/request_context_handler_cpptoc.cc
  cpptoc/request_context_handler_cpptoc.h
  cpptoc/request_handler_cpptoc.cc
  cpptoc/request_handler_cpptoc.h
  cpptoc/resolve_callback_cpptoc.cc
  cpptoc/resolve_callback_cpptoc.h
  cpptoc/resource_bundle_handler_cpptoc.cc
  cpptoc/resource_bundle_handler_cpptoc.h
  cpptoc/resource_handler_cpptoc.cc
  cpptoc/resource_handler_cpptoc.h
  cpptoc/response_filter_cpptoc.cc
  cpptoc/response_filter_cpptoc.h
  cpptoc/run_file_dialog_callback_cpptoc.cc
  cpptoc/run_file_dialog_callback_cpptoc.h
  cpptoc/scheme_handler_factory_cpptoc.cc
  cpptoc/scheme_handler_factory_cpptoc.h
  cpptoc/server_handler_cpptoc.cc
  cpptoc/server_handler_cpptoc.h
  cpptoc/set_cookie_callback_cpptoc.cc
  cpptoc/set_cookie_callback_cpptoc.h
  cpptoc/string_visitor_cpptoc.cc
  cpptoc/string_visitor_cpptoc.h
  cpptoc/task_cpptoc.cc
  cpptoc/task_cpptoc.h
  cpptoc/urlrequest_client_cpptoc.cc
  cpptoc/urlrequest_client_cpptoc.h
  cpptoc/v8accessor_cpptoc.cc
  cpptoc/v8accessor_cpptoc.h
  cpptoc/v8array_buffer_release_callback_cpptoc.cc
  cpptoc/v8array_buffer_release_callback_cpptoc.h
  cpptoc/v8handler_cpptoc.cc
  cpptoc/v8handler_cpptoc.h
  cpptoc/v8interceptor_cpptoc.cc
  cpptoc/v8interceptor_cpptoc.h
  cpptoc/web_plugin_info_visitor_cpptoc.cc
  cpptoc/web_plugin_info_visitor_cpptoc.h
  cpptoc/web_plugin_unstable_callback_cpptoc.cc
  cpptoc/web_plugin_unstable_callback_cpptoc.h
  cpptoc/write_handler_cpptoc.cc
  cpptoc/write_handler_cpptoc.h
  )
source_group(libcef_dll\\\\cpptoc FILES ${LIBCEF_CPPTOC_SRCS})

set(LIBCEF_CPPTOC_TEST_SRCS
  cpptoc/test/translator_test_ref_ptr_client_child_cpptoc.cc
  cpptoc/test/translator_test_ref_ptr_client_child_cpptoc.h
  cpptoc/test/translator_test_ref_ptr_client_cpptoc.cc
  cpptoc/test/translator_test_ref_ptr_client_cpptoc.h
  cpptoc/test/translator_test_scoped_client_child_cpptoc.cc
  cpptoc/test/translator_test_scoped_client_child_cpptoc.h
  cpptoc/test/translator_test_scoped_client_cpptoc.cc
  cpptoc/test/translator_test_scoped_client_cpptoc.h
  )
source_group(libcef_dll\\\\cpptoc\\\\test FILES ${LIBCEF_CPPTOC_TEST_SRCS})

set(LIBCEF_CPPTOC_VIEWS_SRCS
  cpptoc/views/browser_view_delegate_cpptoc.cc
  cpptoc/views/browser_view_delegate_cpptoc.h
  cpptoc/views/button_delegate_cpptoc.cc
  cpptoc/views/button_delegate_cpptoc.h
  cpptoc/views/menu_button_delegate_cpptoc.cc
  cpptoc/views/menu_button_delegate_cpptoc.h
  cpptoc/views/panel_delegate_cpptoc.cc
  cpptoc/views/panel_delegate_cpptoc.h
  cpptoc/views/textfield_delegate_cpptoc.cc
  cpptoc/views/textfield_delegate_cpptoc.h
  cpptoc/views/view_delegate_cpptoc.cc
  cpptoc/views/view_delegate_cpptoc.h
  cpptoc/views/window_delegate_cpptoc.cc
  cpptoc/views/window_delegate_cpptoc.h
  )
source_group(libcef_dll\\\\cpptoc\\\\views FILES ${LIBCEF_CPPTOC_VIEWS_SRCS})

set(LIBCEF_CTOCPP_SRCS
  ctocpp/auth_callback_ctocpp.cc
  ctocpp/auth_callback_ctocpp.h
  ctocpp/before_download_callback_ctocpp.cc
  ctocpp/before_download_callback_ctocpp.h
  ctocpp/binary_value_ctocpp.cc
  ctocpp/binary_value_ctocpp.h
  ctocpp/browser_ctocpp.cc
  ctocpp/browser_ctocpp.h
  ctocpp/browser_host_ctocpp.cc
  ctocpp/browser_host_ctocpp.h
  ctocpp/callback_ctocpp.cc
  ctocpp/callback_ctocpp.h
  ctocpp/command_line_ctocpp.cc
  ctocpp/command_line_ctocpp.h
  ctocpp/context_menu_params_ctocpp.cc
  ctocpp/context_menu_params_ctocpp.h
  ctocpp/cookie_manager_ctocpp.cc
  ctocpp/cookie_manager_ctocpp.h
  ctocpp/ctocpp_ref_counted.h
  ctocpp/ctocpp_scoped.h
  ctocpp/dictionary_value_ctocpp.cc
  ctocpp/dictionary_value_ctocpp.h
  ctocpp/domdocument_ctocpp.cc
  ctocpp/domdocument_ctocpp.h
  ctocpp/domnode_ctocpp.cc
  ctocpp/domnode_ctocpp.h
  ctocpp/download_item_callback_ctocpp.cc
  ctocpp/download_item_callback_ctocpp.h
  ctocpp/download_item_ctocpp.cc
  ctocpp/download_item_ctocpp.h
  ctocpp/drag_data_ctocpp.cc
  ctocpp/drag_data_ctocpp.h
  ctocpp/extension_ctocpp.cc
  ctocpp/extension_ctocpp.h
  ctocpp/file_dialog_callback_ctocpp.cc
  ctocpp/file_dialog_callback_ctocpp.h
  ctocpp/frame_ctocpp.cc
  ctocpp/frame_ctocpp.h
  ctocpp/get_extension_resource_callback_ctocpp.cc
  ctocpp/get_extension_resource_callback_ctocpp.h
  ctocpp/image_ctocpp.cc
  ctocpp/image_ctocpp.h
  ctocpp/jsdialog_callback_ctocpp.cc
  ctocpp/jsdialog_callback_ctocpp.h
  ctocpp/list_value_ctocpp.cc
  ctocpp/list_value_ctocpp.h
  ctocpp/menu_model_ctocpp.cc
  ctocpp/menu_model_ctocpp.h
  ctocpp/navigation_entry_ctocpp.cc
  ctocpp/navigation_entry_ctocpp.h
  ctocpp/post_data_ctocpp.cc
  ctocpp/post_data_ctocpp.h
  ctocpp/post_data_element_ctocpp.cc
  ctocpp/post_data_element_ctocpp.h
  ctocpp/print_dialog_callback_ctocpp.cc
  ctocpp/print_dialog_callback_ctocpp.h
  ctocpp/print_job_callback_ctocpp.cc
  ctocpp/print_job_callback_ctocpp.h
  ctocpp/print_settings_ctocpp.cc
  ctocpp/print_settings_ctocpp.h
  ctocpp/process_message_ctocpp.cc
  ctocpp/process_message_ctocpp.h
  ctocpp/request_callback_ctocpp.cc
  ctocpp/request_callback_ctocpp.h
  ctocpp/request_context_ctocpp.cc
  ctocpp/request_context_ctocpp.h
  ctocpp/request_ctocpp.cc
  ctocpp/request_ctocpp.h
  ctocpp/resource_bundle_ctocpp.cc
  ctocpp/resource_bundle_ctocpp.h
  ctocpp/response_ctocpp.cc
  ctocpp/response_ctocpp.h
  ctocpp/run_context_menu_callback_ctocpp.cc
  ctocpp/run_context_menu_callback_ctocpp.h
  ctocpp/scheme_registrar_ctocpp.cc
  ctocpp/scheme_registrar_ctocpp.h
  ctocpp/select_client_certificate_callback_ctocpp.cc
  ctocpp/select_client_certificate_callback_ctocpp.h
  ctocpp/server_ctocpp.cc
  ctocpp/server_ctocpp.h
  ctocpp/sslinfo_ctocpp.cc
  ctocpp/sslinfo_ctocpp.h
  ctocpp/sslstatus_ctocpp.cc
  ctocpp/sslstatus_ctocpp.h
  ctocpp/stream_reader_ctocpp.cc
  ctocpp/stream_reader_ctocpp.h
  ctocpp/stream_writer_ctocpp.cc
  ctocpp/stream_writer_ctocpp.h
  ctocpp/task_runner_ctocpp.cc
  ctocpp/task_runner_ctocpp.h
  ctocpp/thread_ctocpp.cc
  ctocpp/thread_ctocpp.h
  ctocpp/urlrequest_ctocpp.cc
  ctocpp/urlrequest_ctocpp.h
  ctocpp/v8context_ctocpp.cc
  ctocpp/v8context_ctocpp.h
  ctocpp/v8exception_ctocpp.cc
  ctocpp/v8exception_ctocpp.h
  ctocpp/v8stack_frame_ctocpp.cc
  ctocpp/v8stack_frame_ctocpp.h
  ctocpp/v8stack_trace_ctocpp.cc
  ctocpp/v8stack_trace_ctocpp.h
  ctocpp/v8value_ctocpp.cc
  ctocpp/v8value_ctocpp.h
  ctocpp/value_ctocpp.cc
  ctocpp/value_ctocpp.h
  ctocpp/waitable_event_ctocpp.cc
  ctocpp/waitable_event_ctocpp.h
  ctocpp/web_plugin_info_ctocpp.cc
  ctocpp/web_plugin_info_ctocpp.h
  ctocpp/x509cert_principal_ctocpp.cc
  ctocpp/x509cert_principal_ctocpp.h
  ctocpp/x509certificate_ctocpp.cc
  ctocpp/x509certificate_ctocpp.h
  ctocpp/xml_reader_ctocpp.cc
  ctocpp/xml_reader_ctocpp.h
  ctocpp/zip_reader_ctocpp.cc
  ctocpp/zip_reader_ctocpp.h
  )
source_group(libcef_dll\\\\ctocpp FILES ${LIBCEF_CTOCPP_SRCS})

set(LIBCEF_CTOCPP_TEST_SRCS
  ctocpp/test/translator_test_ctocpp.cc
  ctocpp/test/translator_test_ctocpp.h
  ctocpp/test/translator_test_ref_ptr_library_child_child_ctocpp.cc
  ctocpp/test/translator_test_ref_ptr_library_child_child_ctocpp.h
  ctocpp/test/translator_test_ref_ptr_library_child_ctocpp.cc
  ctocpp/test/translator_test_ref_ptr_library_child_ctocpp.h
  ctocpp/test/translator_test_ref_ptr_library_ctocpp.cc
  ctocpp/test/translator_test_ref_ptr_library_ctocpp.h
  ctocpp/test/translator_test_scoped_library_child_child_ctocpp.cc
  ctocpp/test/translator_test_scoped_library_child_child_ctocpp.h
  ctocpp/test/translator_test_scoped_library_child_ctocpp.cc
  ctocpp/test/translator_test_scoped_library_child_ctocpp.h
  ctocpp/test/translator_test_scoped_library_ctocpp.cc
  ctocpp/test/translator_test_scoped_library_ctocpp.h
  )
source_group(libcef_dll\\\\ctocpp\\\\test FILES ${LIBCEF_CTOCPP_TEST_SRCS})

set(LIBCEF_CTOCPP_VIEWS_SRCS
  ctocpp/views/box_layout_ctocpp.cc
  ctocpp/views/box_layout_ctocpp.h
  ctocpp/views/browser_view_ctocpp.cc
  ctocpp/views/browser_view_ctocpp.h
  ctocpp/views/button_ctocpp.cc
  ctocpp/views/button_ctocpp.h
  ctocpp/views/display_ctocpp.cc
  ctocpp/views/display_ctocpp.h
  ctocpp/views/fill_layout_ctocpp.cc
  ctocpp/views/fill_layout_ctocpp.h
  ctocpp/views/label_button_ctocpp.cc
  ctocpp/views/label_button_ctocpp.h
  ctocpp/views/layout_ctocpp.cc
  ctocpp/views/layout_ctocpp.h
  ctocpp/views/menu_button_ctocpp.cc
  ctocpp/views/menu_button_ctocpp.h
  ctocpp/views/menu_button_pressed_lock_ctocpp.cc
  ctocpp/views/menu_button_pressed_lock_ctocpp.h
  ctocpp/views/panel_ctocpp.cc
  ctocpp/views/panel_ctocpp.h
  ctocpp/views/scroll_view_ctocpp.cc
  ctocpp/views/scroll_view_ctocpp.h
  ctocpp/views/textfield_ctocpp.cc
  ctocpp/views/textfield_ctocpp.h
  ctocpp/views/view_ctocpp.cc
  ctocpp/views/view_ctocpp.h
  ctocpp/views/window_ctocpp.cc
  ctocpp/views/window_ctocpp.h
  )
source_group(libcef_dll\\\\ctocpp\\\\views FILES ${LIBCEF_CTOCPP_VIEWS_SRCS})

set(LIBCEF_WRAPPER_SRCS
  wrapper/cef_browser_info_map.h
  wrapper/cef_byte_read_handler.cc
  wrapper/cef_closure_task.cc
  wrapper/cef_message_router.cc
  wrapper/cef_resource_manager.cc
  wrapper/cef_scoped_temp_dir.cc
  wrapper/cef_stream_resource_handler.cc
  wrapper/cef_xml_object.cc
  wrapper/cef_zip_archive.cc
  wrapper/libcef_dll_wrapper.cc
  wrapper/libcef_dll_wrapper2.cc
  )
set(LIBCEF_WRAPPER_SRCS_MACOSX
  wrapper/cef_library_loader_mac.mm
  wrapper/libcef_dll_dylib.cc
  )
LIBCEF_APPEND_PLATFORM_SOURCES(LIBCEF_WRAPPER_SRCS)
source_group(libcef_dll\\\\wrapper FILES ${LIBCEF_WRAPPER_SRCS})

add_library(${CEF_TARGET}
  ${LIBCEF_SRCS}
  ${LIBCEF_BASE_SRCS}
  ${LIBCEF_CPPTOC_SRCS}
  ${LIBCEF_CPPTOC_TEST_SRCS}
  ${LIBCEF_CPPTOC_VIEWS_SRCS}
  ${LIBCEF_CTOCPP_SRCS}
  ${LIBCEF_CTOCPP_TEST_SRCS}
  ${LIBCEF_CTOCPP_VIEWS_SRCS}
  ${LIBCEF_INCLUDE_SRCS}
  ${LIBCEF_INCLUDE_BASE_SRCS}
  ${LIBCEF_INCLUDE_BASE_INTERNAL_SRCS}
  ${LIBCEF_INCLUDE_CAPI_SRCS}
  ${LIBCEF_INCLUDE_CAPI_TEST_SRCS}
  ${LIBCEF_INCLUDE_CAPI_VIEWS_SRCS}
  ${LIBCEF_INCLUDE_INTERNAL_SRCS}
  ${LIBCEF_INCLUDE_TEST_SRCS}
  ${LIBCEF_INCLUDE_VIEWS_SRCS}
  ${LIBCEF_INCLUDE_WRAPPER_SRCS}
  ${LIBCEF_WRAPPER_SRCS}
  )
#SET_LIBRARY_TARGET_PROPERTIES(${CEF_TARGET})

include_directories(
  ${CEF_INCLUDE_DIRS}
  ${CMAKE_SOURCE_DIR}/cef
)

# Creating the CEF wrapper library. Do not define this for dependent targets.
target_compile_definitions(${CEF_TARGET} PRIVATE -DWRAPPING_CEF_SHARED)

add_definitions(-DNOMINMAX)

# Remove the default "lib" prefix from the resulting library.
set_target_properties(${CEF_TARGET} PROPERTIES PREFIX "")
